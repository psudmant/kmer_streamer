import random
import numpy as np

def get_random_N():
    
    """
    get a random nucleotide 
    """

    rnd = random.random()
    
    weights = np.array([.5,.5,.5,.75])  #A,T,C,G
    cweights = np.cumsum(weights)

    if rnd < cweights[0]:
        return "A"
    elif rnd < cweights[1]:
        return "T"
    elif rnd < cweights[2]:
        return "C"

    return "G"

def get_kmer(n):
    """
    get a kmer
    """
    return "".join([get_random_N() for i in range(n)])


if __name__=="__main__":

    while 1:
        print get_kmer(5)
